﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace WSSC.V4.DMS.FOS.TestSaperion.Log
{
    public class TraceInspector : IDispatchMessageInspector, IClientMessageInspector
    {
        private readonly ILogger _Logger;
        public TraceInspector(ILogger logger)
        {
            _Logger = logger ??
                throw new ArgumentNullException(nameof(logger));
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            _Logger.Write("ClientResponse: " + reply.ToString());
        }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        { 
            _Logger.Write("ServerRequest: " + request.ToString());
            return null;
        }
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            MessageBuffer buffer = reply.CreateBufferedCopy(Int32.MaxValue);
            reply = buffer.CreateMessage();

            _Logger.Write("ServerResponse: " + reply.ToString());
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        { 
            _Logger.Write("ClientRequest: " + request.ToString());
            return null;
        }
    }
}
