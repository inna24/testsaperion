﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSSC.V4.DMS.FOS.TestSaperion.Log
{
    /// <summary>
    /// Логирование в консоль
    /// </summary>
    public class ConsoleLog : ILogger
    { 
        public void Write(string message) =>
            Console.WriteLine(message);

        public void WriteError(string message) =>
            Console.WriteLine(message);
    }
}
