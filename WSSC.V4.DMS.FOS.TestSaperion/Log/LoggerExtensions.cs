﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSSC.V4.DMS.FOS.TestSaperion.Log
{

    /// <summary>
    /// методы расширения для логгера
    /// </summary>
    public static class LoggerExtensions
    { 

        public static void Write(this ILogger logger, object obj) =>
            logger?.Write(obj?.ToString());

        public static void WriteError(this ILogger logger, object obj) =>
            logger?.WriteError(obj?.ToString());


        public static ILogger AddLogger(this ILogger logger1, ILogger logger2)
        {
            if (logger1 is ContainerLoggers container1)
            {
                container1.Add(logger2);
                return container1;
            }

            if (logger2 is ContainerLoggers container2)
            {
                container2.Add(logger1);
                return container2;
            }

            ContainerLoggers container = new ContainerLoggers();

            container.Add(logger1);
            container.Add(logger2);

            return container;
        }
    }
}
