﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WSSC.V4.DMS.FOS.TestSaperion.Log
{
    /// <summary>
    /// Логирование в файл
    /// </summary>
    public class FileLog : ILogger
    {
        private readonly string _folder;
        private readonly string _fileName;

        public FileLog(string folder = null)
        {
            _fileName = $"Log_{DateTime.Now:MM.dd.yy H.mm.ss}.txt";

            if (!string.IsNullOrEmpty(folder))
            {
                if (!Directory.Exists(folder))
                    throw new DirectoryNotFoundException(folder);

                _folder = folder;
                _fileName = Path.Combine(_folder, _fileName);
            }
        }

        public void Write(string message) => File.AppendAllText(_fileName, message + "\r\n");

        public void WriteError(string message) => File.AppendAllText(_fileName, message + "\r\n");
    }
}
