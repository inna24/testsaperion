﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSSC.V4.DMS.FOS.TestSaperion.Log
{
    /// <summary>
    /// Интерфейс логгера
    /// </summary>
    public interface ILogger
    { 
        void Write(string message);
        void WriteError(string message); 
    }
}