﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web; 

namespace WSSC.V4.DMS.FOS.TestSaperion.Log
{
    /// <summary>
    /// Инспектор логов
    /// </summary>
    public class TraceEndpointBehavior : Attribute, IEndpointBehavior, IServiceBehavior, IContractBehavior
    {
        private readonly ILogger _Logger;

        public TraceEndpointBehavior(ILogger logger)
        {
            _Logger = logger ??
                throw new ArgumentNullException(nameof(logger));
        } 

        private void ApplyDispatchBehavior(DispatchRuntime dispatcher)
        {
            // Don't add an error handler if it already exists
            foreach (IDispatchMessageInspector messageInspector in dispatcher.MessageInspectors)
            {
                if (messageInspector is TraceInspector)
                    return;
            }
            TraceInspector inspector = new TraceInspector(_Logger);

            dispatcher.MessageInspectors.Add(inspector);
        }

        private void ApplyClientBehavior(ClientRuntime runtime)
        {
            // Don't add a message inspector if it already exists
            foreach (IClientMessageInspector messageInspector in runtime.MessageInspectors)
            {
                if (messageInspector is TraceInspector)
                    return;
            }

            TraceInspector inspector = new TraceInspector(_Logger);

            runtime.MessageInspectors.Add(inspector);
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        { }

        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        { }
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        { }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        => ApplyClientBehavior(clientRuntime);

        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        => ApplyClientBehavior(clientRuntime);

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        => ApplyDispatchBehavior(endpointDispatcher.DispatchRuntime);

        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        => ApplyDispatchBehavior(dispatchRuntime);

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        { }

        public void Validate(ServiceEndpoint endpoint)
        { }

        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        { }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        { }
    }
}