﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WSSC.V4.DMS.FOS.TestSaperion.Log
{
    /// <summary>
    /// Лонтейнер с логгерами
    /// </summary>
    public class ContainerLoggers : ILogger
    {
        private readonly List<ILogger> _loggers = new List<ILogger>();

        public void Write(string message) => _loggers.ForEach(x => x.Write(message));
        public void WriteError(string message) => _loggers.ForEach(x => x.WriteError(message));

        /// <summary>
        /// Добавить логгер 
        /// </summary>
        /// <param name="logger"></param>
        public void Add(ILogger logger)
        {
            _loggers.Add(logger ??
                throw new ArgumentNullException(nameof(logger))); 
        } 
    }
}
