﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WSSC.V4.DMS.FOS.TestSaperion.Log;
using WSSC.V4.DMS.FOS.TestSaperion.Saperion;

namespace WSSC.V4.DMS.FOS.TestSaperion
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger logger = new ConsoleLog().AddLogger(new FileLog());

            logger.Write("start");
            try
            {
                SaperionOptions saperionOptions = new SaperionOptions()
                {
                    Login = ReadLine("Введите логин"),
                    Password = ReadLine("Введите пароль"),
                    ServiceUrl = "http://172.27.10.245:8733/SaperionWssDocsService/SaperionWssDocsService.svc",
                };


                using (SaperionAdapter adapter = new SaperionAdapter(saperionOptions, logger))
                {
                    string sapBE = "5900";
                    string sapDocID = "8AC06AFFA43ED045BA833C396BE5A4CE";
                    int listId = 464;
                    string partyId = "";
                    DateTime saperionAddDate = new DateTime(2020, 12, 07);


                    GetResult result = adapter.GetSaperionDocs(sapBE, sapDocID, listId, partyId, saperionAddDate);
                    logger.Write(result.SaperionLink);
                }
            }
            catch (Exception ex)
            {
                logger.Write(ex.ToString());
            }
            logger.Write("end");
            Console.ReadLine();
        }

        private static string ReadLine(string message)
        {
            Console.WriteLine(message);

            string value = null;

            while (string.IsNullOrEmpty(value))
                value = Console.ReadLine();

            return value;
        }
    }
}
