﻿namespace WSSC.V4.DMS.FOS.TestSaperion
{
    public class SaperionOptions
    {
        /// <summary>
        /// Логин сервисной учетной записи для авторизации сервиса Saperion.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Адресс подключения
        /// </summary>
        public string ServiceUrl { get; set; }
    }
}
