﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using WSSC.V4.DMS.FOS.TestSaperion.Log;
using WSSC.V4.DMS.FOS.TestSaperion.Saperion;

namespace WSSC.V4.DMS.FOS.TestSaperion
{
    /// <summary>
    /// Класс реализующий работу с веб-сервисом Saperion.
    /// </summary>
    public class SaperionAdapter : IDisposable
    {

        private readonly SaperionOptions _saperionOptions;
        private readonly ILogger _logger;

        public SaperionAdapter(SaperionOptions saperionOptions, ILogger logger)
        {
            _saperionOptions = saperionOptions ??
                throw new ArgumentNullException(nameof(saperionOptions));
            _logger = logger ??
                throw new ArgumentNullException(nameof(logger));
        }

        private bool __init_SapService;
        private SaperionWssDocsServiceClient _SapService;
        /// <summary>
        /// Сервис Saperion.
        /// </summary>
        public SaperionWssDocsServiceClient SapService
        {
            get
            {
                if (!__init_SapService)
                {
                    WSHttpBinding binding = new WSHttpBinding();
                    EndpointAddress address = new EndpointAddress(_saperionOptions.ServiceUrl);
                    binding.MessageEncoding = WSMessageEncoding.Mtom;

                    binding.SendTimeout = TimeSpan.FromMinutes(10);
                    binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
                    binding.Security.Mode = SecurityMode.Message;

                    binding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;

                    _SapService = new SaperionWssDocsServiceClient(binding, address);
                    _SapService.ChannelFactory.Credentials.Windows.ClientCredential =
                        new NetworkCredential(_saperionOptions.Login,
                                              _saperionOptions.Password);

                    _SapService.Endpoint.Behaviors.Add(new TraceEndpointBehavior(_logger));
                    __init_SapService = true;
                }
                return _SapService;
            }
        }

        /// <summary>
        /// Получает от веб-сервиса результат поиска файла в Saperion.
        /// </summary>
        /// <param name="sapBE">Код бизнес-сферы.</param>
        /// <param name="sapDocID">Идентификатор документа в Saperion.</param>
        /// <returns></returns>
        public GetResult GetSaperionDocs(string sapBE, string sapDocID, int listId, string partyId, DateTime saperionAddDate)
        {
            if (string.IsNullOrEmpty(sapBE))
                throw new ArgumentNullException(nameof(sapBE));

            if (string.IsNullOrEmpty(sapDocID))
                throw new ArgumentNullException(nameof(sapDocID));

            if (listId < 1)
                throw new ArgumentNullException(nameof(listId));

            if (saperionAddDate == DateTime.MinValue)
                throw new ArgumentException(nameof(saperionAddDate));

            GetResult resource = SapService.GetSaperionDocs(sapBE, sapDocID, listId, partyId, saperionAddDate.ToString("dd.MM.yyyy"));

            return resource;
        }

        /// <summary>
        /// Загружает файл в Saperion.
        /// </summary>
        /// <param name="ownerID"></param>
        /// <param name="barCode">Бар-код.</param>
        /// <param name="fieldName">Имя поля файлов.</param>
        /// <param name="fileListID">Идентификатор библиотеки файлов.</param>
        /// <param name="fileItemID">Идентификатор элемента файла.</param>
        /// <param name="fileVersionID">Идентификатор версии файла.</param>
        /// <param name="listID">Идентификатор списка.</param>
        /// <param name="itemID">Идентификатор документа.</param>
        /// <param name="regNumber">Регистрационный номер документа.</param>
        /// <param name="regDate">Дата регистрации документа.</param>
        /// <param name="inRegNumber">Рег. номер входящего документа.</param>
        /// <param name="inRegDate">Дата регистрации входящего документа.</param>
        /// <param name="docKind">Вид документа.</param>
        /// <param name="createDate">Дата создания.</param>
        /// <param name="actor"></param>
        /// <param name="state">Статус.</param>
        /// <param name="stage">Этап.</param>
        /// <param name="deal">Дело.</param>
        /// <param name="fileName">Имя файла.</param>
        /// <param name="content">Содержимое файла.</param>
        /// <param name="idLiabilityCenter">ЦФО.</param>
        /// <param name="partyId">Контрагент.</param>
        /// <returns></returns>
        public PutResult PutSaperionDocs(string ownerID, string barCode, string fieldName, Guid fileListID, int fileItemID,
            int fileVersionID, int listID, int itemID, string regNumber, DateTime regDate,
            string inRegNumber, DateTime inRegDate, string docKind, DateTime createDate, string actor,
            string state, string stage, string deal, string fileName, byte[] content, string idLiabilityCenter, string partyId, string oracleID_R11, string oracleID_R12, string parentOracleID_R11, string parentOracleIDR12)
        {
            PutResult resource = null;
            resource = SapService.PutSaperionDocs(ownerID, barCode, fieldName, fileListID, fileItemID, fileVersionID,
                listID, itemID, regNumber, regDate, inRegNumber, inRegDate, docKind, createDate, actor, state, stage,
                deal, fileName, content, idLiabilityCenter, partyId, oracleID_R11, oracleID_R12, parentOracleID_R11, parentOracleIDR12);

            return resource;
        }

        public void Dispose() => (_SapService as IDisposable)?.Dispose();
    }
}
